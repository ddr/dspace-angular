// DUL CUSTOMIZATION: Created this service to track web analytics using Matomo.
// NOTE: DSpace's built-in Angulartics framework does not seem to work
// with the modern versions of Angular and Matomo, so this feature is DIY.
import { Inject, Injectable } from '@angular/core';
import { DOCUMENT } from '@angular/common';
import { Router, NavigationEnd } from '@angular/router';
import { combineLatest } from 'rxjs';
import { ConfigurationDataService } from '../core/data/configuration-data.service';
import { getFirstCompletedRemoteData } from '../core/shared/operators';
import { isEmpty } from '../shared/empty.util';

declare const _paq: any[];

@Injectable({
  providedIn: 'root',
})

export class MatomoAnalyticsService {
  constructor(
    @Inject(DOCUMENT) private document: any,
    private router: Router,
    private configService: ConfigurationDataService,
  ) {}

  /**
   * Track a page view on the initial load, but also track
   * subsequent page views as a user navigates within the app.
   */
  trackPageView(): void {
    if (typeof _paq !== 'undefined') {
      _paq.push(['trackPageView']);
    }

    // Because DSpace-Angular is SPA-like, we need special code to track
    // pageviews when a user navigates to a new "page" within the app.
    // See: https://developer.matomo.org/guides/spa-tracking
    this.router.events.subscribe(event => {
      if (event instanceof NavigationEnd) {
        // The following code will be executed when the URL changes.
        const newUrl = event.url;

        // Get the new URL and page title; tally a pageview in Matomo
        if (typeof _paq !== 'undefined') {
          _paq.push(['setCustomUrl', newUrl]);
          _paq.push(['setDocumentTitle', document.title]);
          _paq.push(['trackPageView']);
        }
      }
    });
  }

  /**
   * Call this method once when Angular initializes on the client side.
   * It requests Matomo config from the backend REST-API (see rest.properties.exposed
   * in dspace.cfg), then adds the tracking snippet to the page. This is loosely based on
   * the current (defunct) app/statistics/google-analytics.service.ts
   */
  addTrackingIdToPage(): void {
    const matomoSiteId$ = this.configService.findByPropertyName('matomo.siteid').pipe(
      getFirstCompletedRemoteData(),
    );
    const matomoHost$ = this.configService.findByPropertyName('matomo.host').pipe(
      getFirstCompletedRemoteData(),
    );
    combineLatest([matomoSiteId$, matomoHost$])
    .subscribe(([matomoSiteIdData, matomoHostData]) => {

      // make sure we got both variables from the backend
      if (!matomoHostData.hasSucceeded && !matomoSiteIdData.hasSucceeded) {
        return;
      }

      const trackingId = matomoSiteIdData.payload?.values[0];
      const matomoHost = matomoHostData.payload?.values[0];

      // make sure we received a tracking id & host value
      if (isEmpty(trackingId) || isEmpty(matomoHost)) {
        return;
      }

      // add tracking script snippet to page
      const keyScript = this.document.createElement('script');
      keyScript.id = 'matomo_analytics';

      // Establish tracking script; note we have removed the
      // _paq.push(['trackPageView']);
      keyScript.innerHTML = `var _paq = window._paq = window._paq || [];
                            _paq.push(['enableLinkTracking']);
                            (function() {
                              var u="//${matomoHost}/";
                              _paq.push(['setTrackerUrl', u+'matomo.php']);
                              _paq.push(['setSiteId', '${trackingId}']);
                              var d = document, g = d.createElement('script'), s=d.getElementsByTagName('script')[0];
                              g.async = true;
                              g.src = u+'matomo.js';
                              s.parentNode.insertBefore(g, s);
                            })();`;
      this.document.body.appendChild(keyScript);
    });
  }
}
