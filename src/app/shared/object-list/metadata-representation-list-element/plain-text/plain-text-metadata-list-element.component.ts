import { MetadataRepresentationType } from '../../../../core/shared/metadata-representation/metadata-representation.model';
import { Component, OnInit } from '@angular/core';
import { MetadataRepresentationListElementComponent } from '../metadata-representation-list-element.component';
import { metadataRepresentationComponent } from '../../../metadata-representation/metadata-representation.decorator';
import { VALUE_LIST_BROWSE_DEFINITION } from '../../../../core/shared/value-list-browse-definition.resource-type';

@metadataRepresentationComponent('Publication', MetadataRepresentationType.PlainText)
// For now, authority controlled fields are rendered the same way as plain text fields
@metadataRepresentationComponent('Publication', MetadataRepresentationType.AuthorityControlled)
@Component({
  selector: 'ds-plain-text-metadata-list-element',
  templateUrl: './plain-text-metadata-list-element.component.html',
})
/**
 * A component for displaying MetadataRepresentation objects in the form of plain text
 * It will simply use the value retrieved from MetadataRepresentation.getValue() to display as plain text
 */
export class PlainTextMetadataListElementComponent extends MetadataRepresentationListElementComponent implements OnInit {

  scholarsProfile$: Promise<string>;

  /** DUL CUSTOMIZATION */
  ngOnInit(): void {
    if (this.mdRepresentation.getAssociatedValue('duke.contributor.id')) {
      this.scholarsProfile$ = this.getScholarsProfile(this.mdRepresentation.getAssociatedValue('duke.contributor.id'));
    }
  }

  /**
   * Get the appropriate query parameters for this browse link, depending on whether the browse definition
   * expects 'startsWith' (eg browse by date) or 'value' (eg browse by title)
   */
  getQueryParams() {
    // DUL CUSTOMIZATION: Always query by value
    let queryParams = {value: this.mdRepresentation.getValue()};
    if (this.mdRepresentation.browseDefinition.getRenderType() === VALUE_LIST_BROWSE_DEFINITION.value) {
      return {value: this.mdRepresentation.getValue()};
    }
    return queryParams;
  }

  /**
   * DUL CUSTOMIZATION Load the Scholars@Duke profile URL
   * @param id the Duke UniqueId
   * @returns the profile URL wrapped in a Promise
   */
  async getScholarsProfile(id: string): Promise<string> {
    try {
      const response = await fetch(`https://scholars.duke.edu/widgets/api/v0.9/people/contact/all.json?uri=${id}`);
      if (response.status === 200) {
        const data = await response.json();
        return data[0].profileURL;
      }
      return Promise.resolve(null);
      } catch (e) {
        return Promise.resolve(null);
      }
  }
}
