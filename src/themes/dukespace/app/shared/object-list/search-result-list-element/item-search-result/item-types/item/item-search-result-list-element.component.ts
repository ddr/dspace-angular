import { Component, OnInit } from '@angular/core';
import {
  listableObjectComponent
} from '../../../../../../../../../app/shared/object-collection/shared/listable-object/listable-object.decorator';
import { ViewMode } from '../../../../../../../../../app/core/shared/view-mode.model';
import {
  ItemSearchResult
} from '../../../../../../../../../app/shared/object-collection/shared/item-search-result.model';
import {
  ItemSearchResultListElementComponent as BaseComponent
} from '../../../../../../../../../app/shared/object-list/search-result-list-element/item-search-result/item-types/item/item-search-result-list-element.component';
import { Observable, catchError, map, of as observableOf } from 'rxjs';
import { AccessStatusObject } from '../../../../../../../../../app/shared/object-collection/shared/badges/access-status-badge/access-status.model';
import { Context } from '../../../../../../../../../app/core/shared/context.model';
import { Item } from '../../../../../../../../../app/core/shared/item.model';
import { hasValue } from '../../../../../../../../../app/shared/empty.util';

@listableObjectComponent('PublicationSearchResult', ViewMode.ListElement, Context.Any, 'dukespace')
@listableObjectComponent(ItemSearchResult, ViewMode.ListElement, Context.Any, 'dukespace')
@Component({
  selector: 'ds-item-search-result-list-element',
  // styleUrls: ['./item-search-result-list-element.component.scss'],
  styleUrls: [
    '../../../../../../../../../app/shared/object-list/search-result-list-element/item-search-result/item-types/item/item-search-result-list-element.component.scss',
    './item-search-result-list-element.component.scss'
  ],
  templateUrl: './item-search-result-list-element.component.html',
  // templateUrl: '../../../../../../../../../app/shared/object-list/search-result-list-element/item-search-result/item-types/item/item-search-result-list-element.component.html',
})
export class ItemSearchResultListElementComponent extends BaseComponent implements OnInit {

  accessStatus$: Observable<string>;

  ngOnInit(): void {
    super.ngOnInit();
    const item = this.object.indexableObject as Item;
    this.accessStatus$ = item.accessStatus?.pipe(
      map((accessStatusRD) => {
        if (accessStatusRD.statusCode !== 401 && hasValue(accessStatusRD.payload)) {
          return accessStatusRD.payload;
        } else {
          return [];
        }
      }),
      map((accessStatus: AccessStatusObject) => hasValue(accessStatus.status) ? accessStatus.status : 'unknown'),
      catchError(() => observableOf('unknown'))
    );
  }
}
