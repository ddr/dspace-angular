import { Component, Inject, Input, OnInit } from '@angular/core';
import { APP_CONFIG, AppConfig } from 'src/config/app-config.interface';
import { Item } from 'src/app/core/shared/item.model';
import { Point } from 'src/app/core/statistics/models/usage-report.model';

@Component({
  selector: 'ds-item-page-statistics-widget',
  templateUrl: './statistics-widget.component.html',
  styleUrls: ['./statistics-widget.component.scss']
})
export class StatisticsWidgetComponent implements OnInit {

  @Input() item: Item;

  views$: Promise<number>;
  downloads$: Promise<number>;

  constructor(@Inject(APP_CONFIG) protected appConfig: AppConfig) {
  }

  ngOnInit(): void {
    this.views$ = this.getViews();
    this.downloads$ = this.getDownloads();
  }

  private async getViews(): Promise<number> {
    try {
      const response = await fetch(this.appConfig.rest.baseUrl + '/api/statistics/usagereports/' + this.item.uuid + '_TotalVisits');
      const data = await response.json();
      return this.sumPoints(data.points);
    } catch (e) {
      return Promise.resolve(0);
    }
  }

  private async getDownloads(): Promise<number> {
    try {
      const response = await fetch(this.appConfig.rest.baseUrl + '/api/statistics/usagereports/' + this.item.uuid + '_TotalDownloads');
      const data = await response.json();
      return this.sumPoints(data.points);
    } catch (e) {
      return Promise.resolve(0);
    }
  }

  private sumPoints(points: Point[]): number {
    return points.reduce((sum: number, point: Point) => sum + this.getPointViews(point.values), 0);
  }

  private getPointViews(values: any): number {
    if (values.views == null) {
      return 0;
    }
    if (Array.isArray(values)) {
      return values[0].views;
    }
    return values.views;
  }
}
