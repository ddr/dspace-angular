import { Component, Input, OnInit } from '@angular/core';
import { Metadata } from 'src/app/core/shared/metadata.utils';
import { MetadataMap } from '../../../../../../../app/core/shared/metadata.models';

@Component({
  selector: 'ds-item-page-citation',
  templateUrl: './citation.component.html',
  styleUrls: ['./citation.component.scss']
})
export class CitationComponent implements OnInit {

  @Input() metadata: MetadataMap;
  citation: string;
  heading: string;
  disclaimer: string;

  ngOnInit(): void {
    const isETD = [ 'dissertation', 'honors thesis', 'masters project',
                  'masters thesis', 'thesis', 'capstone paper',
                  'capstone project', 'course paper']
                  .includes(Metadata.firstValue(this.metadata, 'dc.type')?.toLowerCase().replace(/'/g, ''));
    const isVersionOf = Metadata.firstValue(this.metadata, 'dc.relation.isversionof');
    const preprintFor = Metadata.firstValue(this.metadata, 'duke.preprint.url');
    const isVersionOfDOI = isVersionOf?.startsWith('10') || isVersionOf?.startsWith('doi');
    const type = Metadata.firstValue(this.metadata, 'dc.type');

    /**
     * Generate a citation only for certain item types (using dc.type). Make case-insensitive
     * and apostrophe-agnostic. Omit auto-citation if the item is a version of something else.
     * Typically, this will be a published version that has a DOI (preferred for citing).
     */

    // 1. Journal Article that's a version of a published article w/DOI
    if (type?.toLowerCase() === 'journal article' && isVersionOfDOI) {
      this.heading = 'Publication Info';
      this.citation = `${this.authorList()}${this.year()}${this.title()}. ${this.journalInfo()}${this.isVersionOf(isVersionOf)}${this.retrieved()}`;
      this.disclaimer = this.disclaimerStatement('journal article');
      return;
    }
    // 2. Preprint (may have a preprint URL and/or DOI)
    if (type?.toLowerCase() === 'preprint') {
      this.heading = 'Publication Info';
      this.citation = `${this.authorList()}${this.year()}${this.title()}. ${this.journalInfo()}`;
      this.disclaimer = this.disclaimerStatement('preprint');
      if (preprintFor) {
        this.citation += `Preprint version: ${this.isVersionOf(preprintFor)}`;
      }
      if (isVersionOf) {
        this.citation += `Published version: ${this.isVersionOf(isVersionOf)}`;
      }
      this.citation += `${this.retrieved()}`;
      return;
    }
    // 3. Anything that's a version of something else, e.g., has a published-version DOI or a URL
    if (isVersionOf) {
      return;
    }
    // 4. ETDs (any type that is ETD-like)
    if (isETD) {
      this.heading = 'Citation';
      this.citation = `${this.authorList()}${this.year()}*${this.title()}*. ${this.typeAndInstitution()}${this.retrieved()}`;
      return;
    }
    // 5. Types with TBD citation format, pending future metadata remediation
    if (['book','book review','book section','conference','image','journal article'].includes(type?.toLowerCase())) {
      // Do nothing for now; TBD: explore specific citation formats for these types
      return;
    }
    // 6. No type assigned
    if (!type) {
      // Do nothing
      return;
    }
    // 7. Default / Fallback Citation
    this.heading = 'Citation';
    this.citation = `${this.authorAndCreatorList()}${this.year()}*${this.title()}*. ${this.retrieved()}`;
  }

  private authorList(): string {
    // Truncate lists of seven authors or more; use & for second-to-last item separator
    const first = Metadata.firstValue(this.metadata, 'dc.contributor.author');
    const rest = Metadata.allValues(this.metadata, 'dc.contributor.author')
      .filter((name, index) => {
        return index > 0 && index < 8;
      }).map((name) => {
        const nameParts = name.split(/,\s*/);
        return `${nameParts[1]} ${nameParts[0]}`;
      });
    let result = first;
    if (rest.length > 0) {
      result += ', ';
      if (rest.length < 7) {
        result += ` ${rest.slice(0, -1).join(', ')} and ${rest.slice(-1)}`;
      } else {
        result += ` ${rest.slice(0, 7).join(', ')}, et al.`;
      }
    }
    return result;
  }

  private authorAndCreatorList(): string {
    // Truncate lists of seven author/creators or more; use & for second-to-last item separator
    const list = Metadata.allValues(this.metadata, 'dc.contributor.author')
      .concat(Metadata.allValues(this.metadata, 'dc.creator'));
    const first = list.slice(0, 1)[0];
    const rest = list.filter((name, index) => {
        return index > 0 && index < 8;
      }).map((name) => {
        const nameParts = name.split(/,\s*/);
        return `${nameParts[1]} ${nameParts[0]}`;
      });
    let result = first;
    if (rest.length > 0) {
      result += ', ';
      if (rest.length < 7) {
        result += ` ${rest.slice(0, -1).join(', ')} and ${rest.slice(-1)}`;
      } else {
        result += ` ${rest.slice(0, 7).join(', ')}, et al.`;
      }
    }
    return result;
  }

  private year(): string {
    if (Metadata.firstValue(this.metadata, 'dc.date.created')) {
      return ` (${Metadata.firstValue(this.metadata, 'dc.date.created').substring(0, 4)}). `;
    }
    if (Metadata.firstValue(this.metadata, 'dc.date.issued')) {
      return ` (${Metadata.firstValue(this.metadata, 'dc.date.issued').substring(0, 4)}). `;
    }
    return ' (n.d.). ';
  }

  private title(): string {
    if (Metadata.firstValue(this.metadata, 'dc.title')) {
      // strip any HTML tags and trailing '.'
      return `${Metadata.firstValue(this.metadata, 'dc.title').replace(/<[^>]+>/g, '').replace(/\.$/, '')}`;
    }
    return 'Untitled. ';
  }

  private journalInfo(): string {
    let result = '';
    if (Metadata.firstValue(this.metadata, 'dc.relation.ispartof')) {
      result += `*${Metadata.firstValue(this.metadata, 'dc.relation.ispartof')}*`;
      if (Metadata.firstValue(this.metadata, 'pubs.volume')) {
        result += `, ${Metadata.firstValue(this.metadata, 'pubs.volume')}`;
      }
      if (Metadata.firstValue(this.metadata, 'pubs.issue')) {
        result += `(${Metadata.firstValue(this.metadata, 'pubs.issue')})`;
      }
      if (Metadata.firstValue(this.metadata, 'pubs.begin-page') && Metadata.firstValue(this.metadata, 'pubs.end-page')) {
        result += `. pp. ${Metadata.firstValue(this.metadata, 'pubs.begin-page')}–${Metadata.firstValue(this.metadata, 'pubs.end-page')}. `;
        return result;
      }
      if (Metadata.firstValue(this.metadata, 'pubs.begin-page')) {
        result += `. p. ${Metadata.firstValue(this.metadata, 'pubs.begin-page')}`;
      }

      result += '. ';
    }
    return result;
  }

  private isVersionOf(versionOf: string, text?: string): string {
    const versionText = text ? text : versionOf;
    if (versionOf) {
      if (versionOf.startsWith('http')) {
        return `[${versionText}](${versionOf})`;
      }
      if (versionOf.startsWith('10.') || versionOf.startsWith('doi')) {
        return `[${versionText}](https://doi.org/${versionOf.replace(/\s/g, '')})`;
      }
      return versionOf;
    }
    return '';
  }

  private typeAndInstitution(): string {
    return `<span class="cite-type-institution">${Metadata.firstValue(this.metadata, 'dc.type')}, Duke University. </span>`;
  }

  private retrieved(): string {
    return ` <span class="cite-retrieved">Retrieved from [${Metadata.firstValue(this.metadata, 'dc.identifier.uri')}](${Metadata.firstValue(this.metadata, 'dc.identifier.uri')}).</span>`;
  }

  private disclaimerStatement(type: string): string {
    let reference: string;
    switch (type) {
      case 'preprint':
        reference = this.isVersionOf(Metadata.firstValue(this.metadata, 'duke.preprint.url'), 'citation provided by the preprint service');
        break;
      case 'journal article':
        reference = this.isVersionOf(Metadata.firstValue(this.metadata, 'dc.relation.isversionof'), 'official citation provided by the journal');
        break;
    }
    return `This is constructed from limited available data and may be imprecise. To cite this article, please review & use the ${reference}.`;
  }
}
