import { AfterViewInit, ChangeDetectionStrategy, Component, Inject, OnInit, Renderer2, RendererFactory2 } from '@angular/core';
import { Router } from '@angular/router';
import { DOCUMENT } from '@angular/common';
import { Item } from '../../../../../../../app/core/shared/item.model';
import { ViewMode } from '../../../../../../../app/core/shared/view-mode.model';
import {
  listableObjectComponent
} from '../../../../../../../app/shared/object-collection/shared/listable-object/listable-object.decorator';
import { Context } from '../../../../../../../app/core/shared/context.model';
import { RouteService } from '../../../../../../../app/core/services/route.service';
import {
  UntypedItemComponent as BaseComponent
} from '../../../../../../../app/item-page/simple/item-types/untyped-item/untyped-item.component';
// DUL Customization imports
import { Observable, catchError, map, of as observableOf } from 'rxjs';
import { hasValue } from '../../../../../../../app/shared/empty.util';
import { AccessStatusObject } from '../../../../../../../app/shared/object-collection/shared/badges/access-status-badge/access-status.model';
import { AccessStatusDataService } from '../../../../../../../app/core/data/access-status-data.service';

/**
 * Component that represents an untyped Item page
 */
@listableObjectComponent(Item, ViewMode.StandalonePage, Context.Any, 'dukespace')
@Component({
  selector: 'ds-untyped-item',
  styleUrls: ['./untyped-item.component.scss'],
  // styleUrls: ['../../../../../../../app/item-page/simple/item-types/untyped-item/untyped-item.component.scss'],
  templateUrl: './untyped-item.component.html',
  //templateUrl: '../../../../../../../app/item-page/simple/item-types/untyped-item/untyped-item.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class UntypedItemComponent extends BaseComponent implements AfterViewInit, OnInit {

  private renderer: Renderer2;

  accessStatus$: Observable<string>;
  isAltmetricsAccessible$: Promise<boolean>;
  scholarsProfiles: Array<Promise<object>>;

  constructor(@Inject(DOCUMENT) private _document: any,
    private factory: RendererFactory2,
    protected routeService: RouteService, protected router: Router,
    private accessStatusDataService: AccessStatusDataService) {
    super(routeService, router);
    this.renderer = factory.createRenderer(null, null);
  }

  ngOnInit(): void {
    super.ngOnInit();
    this.isAltmetricsAccessible$ = this.altmetricsIsAccessible();
    this.scholarsProfiles = this.object.allMetadataValues('duke.contributor.id').map((id: string) => {
      return id.substring(id.indexOf('|') + 1);
    }).map((uid: string) => this.getScholarsProfile(uid));
    // Get the access status
    const item = this.object as Item;
    if (item.accessStatus == null) {
      // In case the access status has not been loaded, do it individually.
      item.accessStatus = this.accessStatusDataService.findAccessStatusFor(item);
    }
    this.accessStatus$ = item.accessStatus.pipe(
      map((accessStatusRD) => {
        if (accessStatusRD.statusCode !== 401 && hasValue(accessStatusRD.payload)) {
          return accessStatusRD.payload;
        } else {
          return [];
        }
      }),
      map((accessStatus: AccessStatusObject) => hasValue(accessStatus.status) ? accessStatus.status : 'unknown'),
      catchError(() => observableOf('unknown'))
    );
  }

  ngAfterViewInit(): void {
    // Add Altmetrics badge code
    this.createScriptSrc('https://d1bxh8uas1mnw7.cloudfront.net/assets/embed.js', 'altmetric-embed');
    // Add Dimensions badge code
    this.createScriptSrc('https://badge.dimensions.ai/badge.js', '__dimensions_badge_embed__');
    this.injectScript(`setTimeout( () => {if(window._altmetric_embed_init) {window._altmetric_embed_init();}}, 1000);`, 'altmetric_timeout');
    // Set timeout for Dimensions badge script
    this.injectScript(`setTimeout( () => {window.__dimensions_embed?.addBadges();}, 1000);`, 'dimensions_timeout');
  }

  /**
   * Gets the DOI. For articles, this is usually in the 'dc.relation.isversionof'
   * field, but it may occur in an identifier field instead. If the versionof field
   * is a URL, do not return it.
   * @returns the DOI of the item or of its related article
   */
  getDOIForQuery(): string {
    let doi = this.object.firstMetadataValue('dc.identifier.doi');
    if (!doi) {
      doi = this.object.firstMetadataValue('dc.relation.isversionof');
    }
    if (doi && doi.startsWith('http')) {
      return null;
    }
    return doi;
  }

  /**
   * If there is a DOI in 'dc.relation.isversionof', turn it into an HTML link.
   * @returns an HTML string with the DOI as a link.
   */
  getDOIAsURL(): string {
    const doi = this.object.firstMetadataValue('dc.relation.isversionof');
    if (doi?.startsWith('http')) {
      return `<a href="${doi}">${doi}</a>`;
    }
    if (doi?.startsWith('10.') || doi?.startsWith('doi')) {
      return `<a href="https://doi.org/${doi.replace(/\s/g, '')}">${doi}</a>`;
    }
    return undefined;
  }

  /**
   * DUL CUSTOM FUNCTION
   * Gets the issue date string; simplify any full ISO 8601 datetime
   * values to yyyy-mm-dd but leave other formats alone.
   * E.g., 2009-09-22T17:36:11Z to 2009-09-22
   * @returns a string with the date; time stripped out if present
   */
  shortenIssueDate(): string {
    const dateString = this.object.firstMetadataValue('dc.date.issued');
    const isoDateRegex = /^\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}(\.\d{3})?Z$/;

    return dateString && isoDateRegex.test(dateString) ? dateString.split('T')[0] : dateString;
  }

  /**
   * Gets the article handle. Returns null if there is a DOI, as that's
   * the preferred lookup mechanism.
   * @returns the Handle portion of the dc.identifier.uri or null
   */
  getHandle(): string {
    if (this.object.firstMetadataValue('dc.relation.isversionof')) {
      return null;
    }
    const handle: string = this.object.firstMetadataValue('dc.identifier.uri');
    if (handle && handle.includes('hdl.handle.net/')) {
      return handle.substring(handle.indexOf('hdl.handle.net/') + 15);
    }
    if (handle && handle.includes('/handle/')) {
      return handle.substring(handle.indexOf('/handle/') + 8);
    }
    return null;
  }

 /**
  * Gets the embargo release date metadata value. Note that this alone does not
  * enforce the embargo. The values for this date field may be formatted like:
  * 2028-03-29T00:00:00+01:00
  * 2028-03-29
  * 2028-03
  * 2028
  * @returns the Date the embargo will be lifted
  */
  getEmbargoReleaseDate(): Date {
    const embargoDate: string = this.object.firstMetadataValue('duke.embargo.release');
    if (!embargoDate) {
      return null;
    }
    const embargoDay: string = embargoDate?.split('T')[0];
    const dateParts: Array<number> = embargoDay?.split('-').map(Number);

    // Date object must have the same day as the input string without adjusting for
    // local timezone, so we use this split method instead of casting the string
    // directly to a Date. JS months are 0-indexed, so we subtract 1.
    const parsedDate = new Date(dateParts[0], (dateParts[1] - 1 || 0), (dateParts[2] || 1));

    if (parsedDate) {
      return parsedDate;
    }
    return null;
  }

  /**
   * Altmetrics doesn't know about every DOI or Handle, so we check whether the API query will resolve.
   * @returns whether the handle can be resolved by Altmetrics
   */
  private async altmetricsIsAccessible(): Promise<boolean> {
    const doi = this.getDOIForQuery();
    const controller = new AbortController();
    const timeoutId = setTimeout(() => controller.abort(), 1000);
    try {
      if (doi && !doi.startsWith('http')) {
        const response = await fetch(`https://api.altmetric.com/v1/doi/${doi}`, {method: 'HEAD'});
        return response.status !== 404;
      } else {
        const response = await fetch(`https://api.altmetric.com/v1/handle/${this.getHandle()}`, {method: 'HEAD'});
        return response.status !== 404;
      }
    } catch (e) {
      return Promise.resolve(false);
    } finally {
      clearTimeout(timeoutId);
    }
  }

  /**
   * Adds a script tag with a src attribute at the end of the <body>.
   * @param the URL for the src attribute
   */
  private createScriptSrc(url: string, cssClass: string): void {
    const id = url.replace(/\W/g,'');
    let script = this._document.getElementById(id);
    if (script) {
      script.parentElement.removeChild(script);
    }
    script = this.renderer.createElement('script');
    script.setAttribute('id', id);
    script.setAttribute('type', 'application/javascript');
    script.setAttribute('async','');
    script.setAttribute('charset', 'utf-8');
    script.setAttribute('src', url);
    this.renderer.appendChild(this._document.body, script);
  }

  private injectScript(code: string, codeId?: string) {
    const id = codeId ? codeId : code.replace(/\W/g,'');
    let script = this._document.getElementById(id);
    if (script) {
      script.parentElement.removeChild(script);
    }
    script = this.renderer.createElement('script');
    script.setAttribute('id', id);
    script.setAttribute('type', 'application/javascript');
    const sourcecode = this.renderer.createText(code);
    this.renderer.appendChild(script, sourcecode);
    this.renderer.appendChild(this._document.body, script);
  }

  private async getScholarsProfile(id: string): Promise<object> {
    const controller = new AbortController();
    const timeoutId = setTimeout(() => controller.abort(), 1000);
    try {
      const response = await fetch(`https://scholars.duke.edu/widgets/api/v0.9/people/contact/all.json?uri=${id}`, {signal: controller.signal});
      if (response.status === 200) {
        return response.json();
      } else {
        return Promise.resolve([{}]);
      }
    } catch (e) {
      return Promise.resolve([{}]);
    } finally {
      clearTimeout(timeoutId);
    }
  }
}
