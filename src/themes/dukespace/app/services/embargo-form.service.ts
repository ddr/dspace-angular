import { Inject, Injectable } from '@angular/core';
import { DOCUMENT } from '@angular/common';
import { DynamicFormControlEvent } from '@ng-dynamic-forms/core';

@Injectable({
  providedIn: 'root'
})

export class EmbargoFormService {

  constructor(
    @Inject(DOCUMENT) private _document: any
  ) { }

  /**
   * Handle changes to the embargo length field.
   * If the embargo length is greater than 0, calculate the release
   * date and set the year, month, and day fields. Otherwise, clear them.
   * @param event
   */
  handleEmbargoLengthChange(event: DynamicFormControlEvent) {
    const embargo = event.$event.value;
    const months = Number.parseInt(embargo, 10);
    const message = this._document.querySelector('.dukespace-embargo');
    if (months > 0) {
      const release = new Date();
      release.setMonth(release.getMonth() + Number.parseInt(embargo, 10));
      message.textContent = ' This item will be released on ' + release.toLocaleDateString();
    } else {
      message.textContent = '';
    }
  }
}
