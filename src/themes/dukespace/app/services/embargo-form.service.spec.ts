import { TestBed } from '@angular/core/testing';

import { EmbargoFormService } from './embargo-form.service';

describe('EmbargoFormService', () => {
  let service: EmbargoFormService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(EmbargoFormService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
