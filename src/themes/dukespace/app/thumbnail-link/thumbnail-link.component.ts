import { Component, Input, Inject, OnChanges, SimpleChanges, OnInit } from '@angular/core';
import { Bitstream } from '../../../../app/core/shared/bitstream.model';
import { hasNoValue, hasValue, isNotEmpty } from '../../../../app/shared/empty.util';
import { RemoteData } from '../../../../app/core/data/remote-data';
import { Item } from 'src/app/core/shared/item.model';
import { BehaviorSubject,
         Observable,
         concat as observableConcat,
         map, of as observableOf,
         take,
         EMPTY } from 'rxjs';
import { filter, mergeMap, switchMap } from 'rxjs/operators';
import { FeatureID } from '../../../../app/core/data/feature-authorization/feature-id';
import { AuthorizationDataService } from '../../../../app/core/data/feature-authorization/authorization-data.service';
import { AuthService } from '../../../../app/core/auth/auth.service';
import { FileService } from '../../../../app/core/shared/file.service';
import { APP_CONFIG, AppConfig } from '../../../../config/app-config.interface';
import { BitstreamFormat } from '../../../../app/core/shared/bitstream-format.model';
import { BundleDataService } from '../../../../app/core/data/bundle-data.service';
import { Bundle } from '../../../../app/core/shared/bundle.model';
import { MetadataService } from '../../../../app/core/metadata/metadata.service';
import { followLink } from '../../../../app/shared/utils/follow-link-config.model';
import { getFirstCompletedRemoteData, getFirstSucceededRemoteDataPayload } from 'src/app/core/shared/operators';
import { getDownloadableBitstream } from '../../../../app/core/shared/bitstream.operators';
import { getBitstreamDownloadRoute } from '../../../../app/app-routing-paths';
import { PaginatedList } from '../../../../app/core/data/paginated-list.model';

/**
 * This component renders a given Bitstream as a thumbnail.
 * One input parameter of type Bitstream is expected.
 * If no Bitstream is provided, an HTML placeholder will be rendered instead.
 */
@Component({
  selector: 'ds-thumbnail-link',
  styleUrls: ['./thumbnail-link.component.scss'],
  templateUrl: './thumbnail-link.component.html',
})
export class ThumbnailLinkComponent implements OnChanges, OnInit {
  /**
   * The thumbnail Bitstream
   */
  @Input() thumbnail: Bitstream | RemoteData<Bitstream>;

  /**
   * The default image, used if the thumbnail isn't set or can't be downloaded.
   * If defaultImage is null, a HTML placeholder is used instead.
   */
  @Input() defaultImage? = null;

  @Input() item: Item;

  /**
   * The src attribute used in the template to render the image.
   */
  src$ = new BehaviorSubject<string>(undefined);

  retriedWithToken = false;

  /**
   * i18n key of thumbnail alt text
   */
  @Input() alt? = 'thumbnail.default.alt';

  /**
   * i18n key of HTML placeholder text
   */
  @Input() placeholder? = 'thumbnail.default.placeholder';

  /**
   * Limit thumbnail width to --ds-thumbnail-max-width
   */
  @Input() limitWidth? = true;

  /**
   * Whether the thumbnail is currently loading
   * Start out as true to avoid flashing the alt text while a thumbnail is being loaded.
   */
  isLoading$ = new BehaviorSubject(true);

  /**
   * The link to wrap the thumbnail in, if an Item has been passed in.
   */
  link$: Observable<string>;

  private readonly DOWNLOAD_PDF_URL_MIMETYPES = [
    'application/pdf',                                                          // .pdf
    'application/postscript',                                                   // .ps
    'application/msword',                                                       // .doc
    'application/vnd.openxmlformats-officedocument.wordprocessingml.document',  // .docx
    'application/rtf',                                                          // .rtf
    'application/epub+zip',                                                     // .epub
  ];

  constructor(
    protected auth: AuthService,
    protected authorizationService: AuthorizationDataService,
    protected bundleDataService: BundleDataService,
    protected fileService: FileService,
    protected metadataService: MetadataService,
    @Inject(APP_CONFIG) private appConfig: AppConfig
  ) {
  }

  ngOnInit(): void {
      this.link$ = this.getOriginalLink();
  }

  /**
   * Resolve the thumbnail.
   * Use a default image if no actual image is available.
   */
  ngOnChanges(changes: SimpleChanges): void {
    if (hasNoValue(this.thumbnail)) {
      this.setSrc(this.defaultImage);
      return;
    }

    const src = this.contentHref;
    if (hasValue(src)) {
      this.setSrc(src);
    } else {
      this.setSrc(this.defaultImage);
    }
  }

  /**
   * The current thumbnail Bitstream
   * @private
   */
  private get bitstream(): Bitstream {
    if (this.thumbnail instanceof Bitstream) {
      return this.thumbnail as Bitstream;
    } else if (this.thumbnail instanceof RemoteData) {
      return (this.thumbnail as RemoteData<Bitstream>).payload;
    }
  }

  private get contentHref(): string | undefined {
    if (this.thumbnail instanceof Bitstream) {
      return this.thumbnail?._links?.content?.href;
    } else if (this.thumbnail instanceof RemoteData) {
      return this.thumbnail?.payload?._links?.content?.href;
    }
  }

  /**
   * Handle image download errors.
   * If the image can't be loaded, try re-requesting it with an authorization token in case it's a restricted Bitstream
   * Otherwise, fall back to the default image or a HTML placeholder
   */
  errorHandler() {
    const src = this.src$.getValue();
    const thumbnail = this.bitstream;
    const thumbnailSrc = thumbnail?._links?.content?.href;

    if (!this.retriedWithToken && hasValue(thumbnailSrc) && src === thumbnailSrc) {
      // the thumbnail may have failed to load because it's restricted
      //   → retry with an authorization token
      //     only do this once; fall back to the default if it still fails
      this.retriedWithToken = true;

      this.auth.isAuthenticated().pipe(
        switchMap((isLoggedIn) => {
          if (isLoggedIn) {
            return this.authorizationService.isAuthorized(FeatureID.CanDownload, thumbnail.self);
          } else {
            return observableOf(false);
          }
        }),
        switchMap((isAuthorized) => {
          if (isAuthorized) {
            return this.fileService.retrieveFileDownloadLink(thumbnailSrc);
          } else {
            return observableOf(null);
          }
        })
      ).subscribe((url: string) => {
        if (hasValue(url)) {
          // If we got a URL, try to load it
          //   (if it still fails this method will be called again, and we'll fall back to the default)
          // Otherwise, fall back to the default image right now
          this.setSrc(url);
        } else {
          this.setSrc(this.defaultImage);
        }
      });
    } else {
      if (src !== this.defaultImage) {
        // we failed to get thumbnail (possibly retried with a token but failed again)
        this.setSrc(this.defaultImage);
      } else {
        // we have failed to retrieve the default image, fall back to the placeholder
        this.setSrc(null);
      }
    }
  }

  /**
   * Set the thumbnail.
   * Stop the loading animation if setting to null.
   * @param src
   */
  setSrc(src: string): void {
    this.src$.next(src);
    if (src === null) {
      this.isLoading$.next(false);
    }
  }

  /**
   * Stop the loading animation once the thumbnail is successfully loaded
   */
  successHandler() {
    this.isLoading$.next(false);
  }

  /**
   * The following functionality is mostly copied from
   * /src/app/core/metadata/metadata.service.ts, which loads a meta link to the Item
   * content. Unfortunately, that info is thrown away before we get to the content of
   * the item page, so we re-create it here.
   * @returns an Observable of the URL to the item content
   */
  private getOriginalLink(): Observable<string> {
    if (this.item) {
      // Retrieve the ORIGINAL bundle for the item
      return this.bundleDataService.findByItemAndName(
        this.item,
        'ORIGINAL',
        true,
        true,
        followLink('primaryBitstream'),
        followLink('bitstreams', {
            findListOptions: {
              // limit the number of bitstreams used to find the citation pdf url to the number
              // shown by default on an item page
              elementsPerPage: this.appConfig.item.bitstream.pageSize
            }
        }, followLink('format')),
      ).pipe(
        getFirstSucceededRemoteDataPayload(),
        switchMap((bundle: Bundle) =>
          // First try the primary bitstream
          bundle.primaryBitstream.pipe(
            getFirstCompletedRemoteData(),
            map((rd: RemoteData<Bitstream>) => {
              if (hasValue(rd.payload)) {
                return rd.payload;
              } else {
                return null;
              }
            }),
            getDownloadableBitstream(this.authorizationService),
            // return the bundle as well so we can use it again if there's no primary bitstream
            map((bitstream: Bitstream) => [bundle, bitstream])
          )
        ),
        switchMap(([bundle, primaryBitstream]: [Bundle, Bitstream]) => {
          if (hasValue(primaryBitstream)) {
            // If there was a downloadable primary bitstream, emit its link
            return [getBitstreamDownloadRoute(primaryBitstream)];
          } else {
            // Otherwise consider the regular bitstreams in the bundle
            return bundle.bitstreams.pipe(
              getFirstCompletedRemoteData(),
              switchMap((bitstreamRd: RemoteData<PaginatedList<Bitstream>>) => {
                if (hasValue(bitstreamRd.payload) && bitstreamRd.payload.totalElements === 1) {
                  // If there's only one bitstream in the bundle, emit its link if its downloadable
                  return this.getBitLinkIfDownloadable(bitstreamRd.payload.page[0], bitstreamRd);
                } else {
                  // Otherwise check all bitstreams to see if one matches the format whitelist
                  return this.getFirstAllowedFormatBitstreamLink(bitstreamRd);
                }
              })
            );
          }
        }),
        take(1)
      );
    }
  }

  getBitLinkIfDownloadable(bitstream: Bitstream, bitstreamRd: RemoteData<PaginatedList<Bitstream>>): Observable<string> {
    return observableOf(bitstream).pipe(
      getDownloadableBitstream(this.authorizationService),
      switchMap((bit: Bitstream) => {
        if (hasValue(bit)) {
          return [getBitstreamDownloadRoute(bit)];
        } else {
          // Otherwise check all bitstreams to see if one matches the format whitelist
          return this.getFirstAllowedFormatBitstreamLink(bitstreamRd);
        }
      })
    );
  }

    /**
   * For Items with more than one Bitstream (and no primary Bitstream), link to the first Bitstream
   * with a MIME type.
   *
   * Note this will only check the current page (page size determined item.bitstream.pageSize in the
   * config) of bitstreams for performance reasons.
   * See https://github.com/DSpace/DSpace/issues/8648 for more info
   *
   * included in {@linkcode DOWNLOAD_PDF_URL_MIMETYPES}
   * @param bitstreamRd
   * @private
   */
    private getFirstAllowedFormatBitstreamLink(bitstreamRd: RemoteData<PaginatedList<Bitstream>>): Observable<string> {
      if (hasValue(bitstreamRd.payload) && isNotEmpty(bitstreamRd.payload.page)) {
        // Retrieve the formats of all bitstreams in the page sequentially
        return observableConcat(
          ...bitstreamRd.payload.page.map((bitstream: Bitstream) => bitstream.format.pipe(
            getFirstSucceededRemoteDataPayload(),
            // Keep the original bitstream, because it, not the format, is what we'll need
            // for the link at the end
            map((format: BitstreamFormat) => [bitstream, format])
          ))
        ).pipe(
          // Verify that the bitstream is downloadable
          mergeMap(([bitstream, format]: [Bitstream, BitstreamFormat]) => observableOf(bitstream).pipe(
            getDownloadableBitstream(this.authorizationService),
            map((bit: Bitstream) => [bit, format])
          )),
          // Filter out only pairs with whitelisted formats and non-null bitstreams, null from download check
          filter(([bitstream, format]: [Bitstream, BitstreamFormat]) =>
            hasValue(format) && hasValue(bitstream) && this.DOWNLOAD_PDF_URL_MIMETYPES.includes(format.mimetype)),
          // We only need 1
          take(1),
          // Emit the link of the match
          // tap((v) => console.log('result', v)),
          map(([bitstream, ]: [Bitstream, BitstreamFormat]) => getBitstreamDownloadRoute(bitstream))
        );
      } else {
        return EMPTY;
      }
    }
}
