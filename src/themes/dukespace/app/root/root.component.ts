import { Component } from '@angular/core';
import { slideSidebarPadding } from '../../../../app/shared/animations/slide';
import { RootComponent as BaseComponent } from '../../../../app/root/root.component';
import { Router } from '@angular/router';
import { Inject } from '@angular/core';
import { DOCUMENT } from '@angular/common';
import { CSSVariableService } from '../../../../app/shared/sass-helper/css-variable.service';
import { MenuService } from '../../../../app/shared/menu/menu.service';
import { HostWindowService } from '../../../../app/shared/host-window.service';
import { NativeWindowRef, NativeWindowService } from '../../../../app/core/services/window.service';

@Component({
  selector: 'ds-root',
  // styleUrls: ['./root.component.scss'],
  styleUrls: ['../../../../app/root/root.component.scss'],
  templateUrl: './root.component.html',
  // templateUrl: '../../../../app/root/root.component.html',
  animations: [slideSidebarPadding],
})
export class RootComponent extends BaseComponent {
  // We need to inject _document into RootComponent
  // so that we can use it client-side or server-side
  // else we see this error in prod-mode SSR:
  // ERROR ReferenceError: document is not defined at Object.next
  constructor(
    @Inject(DOCUMENT) private _document: any,
    @Inject(NativeWindowService) _window: NativeWindowRef,
    router: Router,
    cssService: CSSVariableService,
    menuService: MenuService,
    windowService: HostWindowService
  ) {
    super(router, cssService, menuService, windowService);
  }
  // DUL CUSTOM functions for skip links (a11y)
  // TODO: if DSpace core ever adds better skip links, it'd be
  // good for future maintenance to remove this custom component
  skipToLink(id) {
    const element = this._document.getElementById(id);
    element.setAttribute('tabindex','-1');
    element.focus();
    return false;
  }

  skipToComponent(element) {
    // get the first instance of the component
    const ng_component = this._document.querySelector(element);
    ng_component.setAttribute('tabindex','-1');
    ng_component.focus();
    return false;
  }

}
