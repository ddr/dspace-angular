SHELL = /bin/bash

build_tag ?= dspace-angular

.PHONY: build
build:
	docker build -t $(build_tag) .

.PHONY: test
test:
	docker run $(build_tag) sh -c 'yarn run lint --quiet && \
		yarn run check-circ-deps && \
		yarn run build:prod'
